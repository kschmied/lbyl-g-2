//
//  LHETruthAnalysis.hpp
//  LbyLAnalysis
//
//  Created by Kristof Schmieden on 31.01.17.
//  Copyright © 2017 Kristof Schmieden. All rights reserved.
//

#ifndef LHETruthAnalysis_hpp
#define LHETruthAnalysis_hpp

#include <stdio.h>
#include <string>
#include <TChain.h>
#include <TTree.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TClonesArray.h>
#include "ExRootAnalysis/ExRootTreeReader.h"
#include "ExRootAnalysis/ExRootClasses.h"


class LHETruthAnalysis
{
public:
  LHETruthAnalysis();
  ~LHETruthAnalysis(){;}
  
  void SetDumpNEvents(int v){m_dumpNEvents = v;}
  void eventLoop();
  void run(std::vector<std::string> inFile, std::string outFile);
  void finalizeHistograms();
  
private:
  void createHistograms();
  void beautifyPlots();
  void deleteHistograms(){;}
  void connectBranches(std::vector<std::string> inputFileName);

  private:
  bool doPhotonSmearing;
  int m_dumpNEvents;
  std::string m_outFile;
  TChain *m_chain;
  TTree *m_outTree;
  TFile *m_outFileNTUP;
  ExRootTreeReader *treeReader;
  TClonesArray *branchEvent;
  TClonesArray *branchParticle;
  std::vector<TH1 *>m_histos;
  std::vector<TH2 *>m_histos2D;
  std::vector<float> m_vecPhotonPt;
  std::vector<float> m_vecPhotonEta;
  std::vector<float> m_vecPhotonPhi;
  
private:
  TH1* h_DiffractiveT;
  TH1* h_DiffractiveXi;
  TH1* h_DiffractiveXiAcc;
  
  TH1* h_NPhotons;
  TH1* h_Photon_pT;
  TH1* h_AveragePhotonPT;
  TH1* h_DiPhoton_pz;
  TH1* h_DiPhoton_Y;
  TH1* h_Photon_phi;
  TH1* h_Photon_eta;
  TH1* h_DiPhoton_M, *h_DiPhoton_M_P5_Eta25, *h_DiPhoton_M_P5_Eta47, *h_DiPhoton_M_P4_Eta25, *h_DiPhoton_M_P4_Eta47;
  TH1* h_DiPhoton_M_P1_Eta25, *h_DiPhoton_M_P1_Eta47;
  TH1* h_DiPhoton_M_P2_Eta25, *h_DiPhoton_M_P3_Eta25;
  TH1* h_DiPhoton_pT;
  TH1* h_DiPhoton_Aco;
  TH1* h_DiPhoton_dPhi;
  TH1* h_DiPhoton_dEta;
  TH1* h_DiPhoton_dR;
  
  TH2* h_DiPhoton_M_Y;
  TH2* h_DiPhoton_M_Pz;
  TH2* h_DiPhoton_M_Pt;
  TH2* h_DiPhoton_M_Aco;
  TH2* h_Photon_pT_P;
  TH2* h_Photon_eta_P;

  
  TH1* h_AlpM;
  
  

};


#endif /* LHETruthAnalysis_hpp */

