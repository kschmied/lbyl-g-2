//
//  LHETruthAnalysis.cpp
//  LbyLAnalysis
//
//  Created by Kristof Schmieden on 31.01.17.
//  Copyright © 2017 Kristof Schmieden. All rights reserved.
//


#include "LHETruthAnalysis.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <iomanip>
#include <vector>
#include <chrono>
#include <cmath>
#include <string>

#include <TLorentzVector.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TMath.h>
#include <TRandom3.h>

#define EBEAM 6500

using std::cout;
using std::endl;



LHETruthAnalysis::LHETruthAnalysis() : m_outFile("histograms.root")

{
  doPhotonSmearing = false;

}


void LHETruthAnalysis::connectBranches (std::vector<std::string> inputFileName)
{
  m_chain = new TChain("LHEF");
  
  for (unsigned int i = 0; i < inputFileName.size(); i++){
    if (m_chain->Add(inputFileName[i].c_str()) == 0){
      std::cout << "Could not open file "<<inputFileName[i]<<" or did not find TTree"<<std::endl;
      return;
    }
  }
  
  // connect ExRoot Objects
  treeReader = new ExRootTreeReader(m_chain);
  branchEvent = treeReader->UseBranch("Event");
  branchParticle = treeReader->UseBranch("Particle");
  
  // output file
  m_outFileNTUP = new TFile("outputNTUP.root", "RECREATE");
  m_outTree = new TTree("NTUP","TestTree");
  m_outTree->Branch("Photon_pt", &m_vecPhotonPt);
  m_outTree->Branch("Photon_eta", &m_vecPhotonEta);
  m_outTree->Branch("Photon_phi", &m_vecPhotonPhi);

  
}

void LHETruthAnalysis::createHistograms(){
  int nBins_mass = 100;
  float minBin_mass = 0;
  float maxBin_mass = 20;
  
  h_NPhotons = new TH1F("NPhotons", "NPhotons", 11, -0.5, 10.5);
  m_histos.push_back(h_NPhotons);
  h_Photon_pT = new TH1F("PhotonPt", "PhotonPt", 500, 0, 50);
  h_Photon_pT->GetXaxis()->SetTitle("pT[GeV]");
  m_histos.push_back(h_Photon_pT);
  h_AveragePhotonPT = new TH1F("AveragePhotonPT", "AveragePhotonPT", 500, 0, 50);
  h_AveragePhotonPT->GetXaxis()->SetTitle("(pT_{1}+pT_{2})/2 [GeV]");
  m_histos.push_back(h_AveragePhotonPT);
  h_Photon_phi = new TH1F("PhotonPhi", "PhotonPhi", 80, -3.2, 3.2);
  h_Photon_phi->GetXaxis()->SetTitle("#phi");
  m_histos.push_back(h_Photon_phi);
  h_Photon_eta = new TH1F("PhotonEta", "PhotonEta", 50, -5, 5);
  h_Photon_eta->GetXaxis()->SetTitle("#eta");
  m_histos.push_back(h_Photon_eta);
  
  
  h_Photon_pT_P = new TH2F("Photon_pT_P", "Photon_pT_P", 500, 0, 50, 500, 0, 50);
  h_Photon_pT_P->GetYaxis()->SetTitle("P_{#gamma}[GeV]");
  h_Photon_pT_P->GetXaxis()->SetTitle("pT_{#gamma} [GeV]");
  m_histos2D.push_back(h_Photon_pT_P);

  h_Photon_eta_P = new TH2F("Photon_eta_pT", "Photon_eta_pT", 50, 0, 5, 500, 0, 50);
  h_Photon_eta_P->GetYaxis()->SetTitle("pT_{#gamma}[GeV]");
  h_Photon_eta_P->GetXaxis()->SetTitle("eta_{#gamma}");
  m_histos2D.push_back(h_Photon_eta_P);

  h_DiPhoton_M_Y = new TH2F("DiPhotonMassRapidity", "DiPhotonMassRapidity", 20, 0, 60, 30, -3, 3);
  h_DiPhoton_M_Y->GetXaxis()->SetTitle("M_{#gamma#gamma}[GeV]");
  h_DiPhoton_M_Y->GetYaxis()->SetTitle("Y_{#gamma#gamma}");
  m_histos2D.push_back(h_DiPhoton_M_Y);
  
  h_DiPhoton_M_Pz = new TH2F("DiPhotonMassPz", "DiPhotonMassPz", 20, 0, 60,  400, 0, 40);
  h_DiPhoton_M_Pz->GetXaxis()->SetTitle("M_{#gamma#gamma}[GeV]");
  h_DiPhoton_M_Pz->GetYaxis()->SetTitle("Pz_{#gamma#gamma} [GeV]");
  m_histos2D.push_back(h_DiPhoton_M_Pz);

  h_DiPhoton_M_Pt = new TH2F("DiPhotonMassPt", "DiPhotonMassPt", 20, 0, 60,  40, 0, 0.5);
  h_DiPhoton_M_Pt->GetXaxis()->SetTitle("M_{#gamma#gamma}[GeV]");
  h_DiPhoton_M_Pt->GetYaxis()->SetTitle("Pt_{#gamma#gamma} [GeV]");
  m_histos2D.push_back(h_DiPhoton_M_Pt);
  
  h_DiPhoton_M_Aco = new TH2F("DiPhotonMassAco", "DiPhotonMassAco", 20, 0, 60, 200, 0, 0.1);
  h_DiPhoton_M_Aco->GetXaxis()->SetTitle("M_{#gamma#gamma}[GeV]");
  h_DiPhoton_M_Aco->GetYaxis()->SetTitle("Acoplanarity");
  m_histos2D.push_back(h_DiPhoton_M_Aco);
  
/*

   h_DiPhoton_M_P5_Eta25 = new TH1F("DiPhotonMass_P5_Eta25", "DiPhotonMass", nBins_mass, minBin_mass, maxBin_mass);
  h_DiPhoton_M_P5_Eta25->GetXaxis()->SetTitle("M_{#gamma#gamma}[GeV]");
  m_histos.push_back(h_DiPhoton_M_P5_Eta25);
  
  h_DiPhoton_M_P5_Eta47 = new TH1F("DiPhotonMass_P5_Eta47", "DiPhotonMass", nBins_mass, minBin_mass, maxBin_mass);
  h_DiPhoton_M_P5_Eta47->GetXaxis()->SetTitle("M_{#gamma#gamma}[GeV]");
  m_histos.push_back(h_DiPhoton_M_P5_Eta47);
  
  h_DiPhoton_M_P4_Eta25 = new TH1F("DiPhotonMass_P4_Eta25", "DiPhotonMass", nBins_mass, minBin_mass, maxBin_mass);
  h_DiPhoton_M_P4_Eta25->GetXaxis()->SetTitle("M_{#gamma#gamma}[GeV]");
  m_histos.push_back(h_DiPhoton_M_P4_Eta25);
  
  h_DiPhoton_M_P4_Eta47 = new TH1F("DiPhotonMass_P4_Eta47", "DiPhotonMass", nBins_mass, minBin_mass, maxBin_mass);
  h_DiPhoton_M_P4_Eta47->GetXaxis()->SetTitle("M_{#gamma#gamma}[GeV]");
  m_histos.push_back(h_DiPhoton_M_P4_Eta47);

  h_DiPhoton_M_P1_Eta25 = new TH1F("DiPhotonMass_P1_Eta25", "DiPhotonMass", nBins_mass, minBin_mass, maxBin_mass);
  h_DiPhoton_M_P1_Eta25->GetXaxis()->SetTitle("M_{#gamma#gamma}[GeV]");
  m_histos.push_back(h_DiPhoton_M_P1_Eta25);
  
  h_DiPhoton_M_P1_Eta47 = new TH1F("DiPhotonMass_P1_Eta47", "DiPhotonMass", nBins_mass, minBin_mass, maxBin_mass);
  h_DiPhoton_M_P1_Eta47->GetXaxis()->SetTitle("M_{#gamma#gamma}[GeV]");
  m_histos.push_back(h_DiPhoton_M_P1_Eta47);
  
  h_DiPhoton_M_P2_Eta25 = new TH1F("DiPhotonMass_P2_Eta25", "DiPhotonMass", nBins_mass, minBin_mass, maxBin_mass);
  h_DiPhoton_M_P2_Eta25->GetXaxis()->SetTitle("M_{#gamma#gamma}[GeV]");
  m_histos.push_back(h_DiPhoton_M_P2_Eta25);
  
  h_DiPhoton_M_P3_Eta25 = new TH1F("DiPhotonMass_P3_Eta25", "DiPhotonMass", nBins_mass, minBin_mass, maxBin_mass);
  h_DiPhoton_M_P3_Eta25->GetXaxis()->SetTitle("M_{#gamma#gamma}[GeV]");
  m_histos.push_back(h_DiPhoton_M_P3_Eta25);
*/
  
  h_DiPhoton_M = new TH1F("DiPhotonMass", "DiPhotonMass", nBins_mass, minBin_mass, maxBin_mass);
  h_DiPhoton_M->GetXaxis()->SetTitle("M_{#gamma#gamma}[GeV]");
  m_histos.push_back(h_DiPhoton_M);
  h_DiPhoton_pT = new TH1F("DiPhotonPt", "DiPhotonPt", 500, 0, 20);
  h_DiPhoton_pT->GetXaxis()->SetTitle("pT_{#gamma#gamma}[GeV]");
  m_histos.push_back(h_DiPhoton_pT);
  h_DiPhoton_pz = new TH1F("DiPhotonPz", "DiPhotonPz", 500, 0, 5000);
  h_DiPhoton_pT->GetXaxis()->SetTitle("pz_{#gamma#gamma}[GeV]");
  m_histos.push_back(h_DiPhoton_pz);
  h_DiPhoton_Y = new TH1F("DiPhotonY", "DiPhotonY", 50, -5, 5);
  h_DiPhoton_Y->GetXaxis()->SetTitle("Rapidity_{#gamma#gamma}");
  m_histos.push_back(h_DiPhoton_Y);
  h_DiPhoton_Aco = new TH1F("DiPhotonAco", "DiPhotonAco", 200, 0, 0.1);
  h_DiPhoton_Aco->GetXaxis()->SetTitle("Acoplanarity");
  m_histos.push_back(h_DiPhoton_Aco);
  h_DiPhoton_dPhi = new TH1F("DiPhoton_dPhi", "DiPhoton_dPhi", 320, 0, 6.4);
  h_DiPhoton_dPhi->GetXaxis()->SetTitle("#Delta#phi");
  m_histos.push_back(h_DiPhoton_dPhi);
  h_DiPhoton_dEta = new TH1F("DiPhoton_dEta", "DiPhoton_dEta", 100, 0, 10);
  h_DiPhoton_dEta->GetXaxis()->SetTitle("#Delta#eta");
  m_histos.push_back(h_DiPhoton_dEta);
  h_DiPhoton_dR = new TH1F("DiPhoton_dR", "DiPhoton_dR", 100, 0, 10);
  h_DiPhoton_dR->GetXaxis()->SetTitle("#Delta R");
  m_histos.push_back(h_DiPhoton_dR);
  
  h_DiffractiveT = new TH1F("DiffractiveT", "DiffractiveT", 100, 0, 5);
  h_DiffractiveT->GetXaxis()->SetTitle("-t[GeV^2]");
  m_histos.push_back(h_DiffractiveT);
  // AFp acceptance in terms of 0.02 < xi = (E_beam - E_p(detected)) / E_beam < 0.12
  h_DiffractiveXi = new TH1F("DiffractiveXi", "DiffractiveXi", 100, 0, 1);
  h_DiffractiveXi->GetXaxis()->SetTitle("#xi");
  m_histos.push_back(h_DiffractiveXi);
  h_DiffractiveXiAcc = new TH1F("DiffractiveXiAcc", "DiffractiveXiAcc", 3, -0.5, 2.5);
  h_DiffractiveXiAcc->GetXaxis()->SetTitle("## p-Tags");
  h_DiffractiveXiAcc->GetYaxis()->SetTitle("Efficiency");
  m_histos.push_back(h_DiffractiveXiAcc);

  
  h_AlpM = new TH1F("AlpM", "AlpM", 500, 0, 100);
  m_histos.push_back(h_AlpM);
  
  beautifyPlots();
}

void LHETruthAnalysis::beautifyPlots(){
  float labelSize = 0.05;

  for (auto h : m_histos){
    h->GetXaxis()->SetLabelSize(labelSize);
    h->GetYaxis()->SetLabelSize(labelSize);
    h->GetXaxis()->SetTitleSize(labelSize);
    h->GetYaxis()->SetTitleSize(labelSize);

  }
}

/** \brief event loop: does event decoding and histogramming 
 */

void LHETruthAnalysis::eventLoop ()
{
  double eventWeight;
  TRandom3 *rand = new TRandom3();
  rand->SetSeed(3);
  
  std::chrono::time_point<std::chrono::system_clock> now,past;
  int eventsUntilTimeUpdate = 10000;
  int eventsSinceTimeUpdate = -1;
  now = std::chrono::system_clock::now();
  for(int i=0; i<=treeReader->GetEntries()-1; i++) {
    m_vecPhotonPt.clear();
    m_vecPhotonEta.clear();
    m_vecPhotonPhi.clear();

    // do some time accounting
    eventsSinceTimeUpdate++;
    if (eventsSinceTimeUpdate == eventsUntilTimeUpdate){
      past = now;
      now = std::chrono::system_clock::now();
      std::chrono::duration<double> elapsedSeconds = now-past;
      if (elapsedSeconds.count() == 0) {
        cout << "elapsed time == 0 !!! Should not happen"<<endl;
        eventsUntilTimeUpdate *= 2;
      } else {
        double rate = eventsSinceTimeUpdate/(elapsedSeconds.count()*1000);
        eventsSinceTimeUpdate = 0;
        if (elapsedSeconds.count() < 3){
          eventsUntilTimeUpdate *= 5/elapsedSeconds.count();
        } else if (elapsedSeconds.count() > 7) {
          eventsUntilTimeUpdate *= 5/elapsedSeconds.count();
        }
        cout << "Processing event "<<i<<", current event rate: "<< rate << " kHz, elapsed time "<<elapsedSeconds.count() <<endl;
      }
      // if (i > 400001) break;
    }
    
    eventWeight = 1;
    
    /// get ZBoson and decay leptons from event
    treeReader->ReadEntry(i);
    TRootLHEFEvent *event=(TRootLHEFEvent*) branchEvent->At(0);
    
    // If requested, dump full information about fist N events
    if (m_dumpNEvents > 0){
      std::cout << "==================================== "<<std::endl
      << " Event Dump of event "<<i<<std::endl;
      for(int j=0; j < event->Nparticles; j++){
        TRootLHEFParticle *particle=(TRootLHEFParticle*) branchParticle->At(j);
        std::cout << "PID = "<<particle->PID<< ", Status = "<<particle->Status<< ", Mother1 = "<<particle->Mother1 << ", PT = "<<particle->PT<<", E = "<<particle->E<<std::endl;
      }
      m_dumpNEvents--;
    }
    
    // loop over all truth particles
    int nAFpAccPhotons = 0;
    std::vector<TLorentzVector> v_photons;
    for(int j=0; j < event->Nparticles; j++){
      TRootLHEFParticle *particle=(TRootLHEFParticle*) branchParticle->At(j);
      if (particle->PID == 22){
        if (particle->Status == 1){ // select final state photons
          TLorentzVector tlv;
          tlv.SetPtEtaPhiM(particle->PT, particle->Eta, particle->Phi, 0);
          h_Photon_pT_P->Fill(tlv.Pt(), tlv.P());
          
          //if (tlv.P() > 5 && fabs(tlv.Eta()) < 5){
          h_Photon_eta_P->Fill(tlv.Eta(), tlv.Pt());
          v_photons.push_back(tlv);
          h_Photon_pT->Fill(particle->PT);
          h_Photon_phi->Fill(particle->Phi);
          h_Photon_eta->Fill(particle->Eta);
          m_vecPhotonPt.push_back(particle->PT);
          m_vecPhotonEta.push_back(particle->Eta);
          m_vecPhotonPhi.push_back(particle->Phi);

          //}
        } else if (particle->Status == -1) { // initial state photon
          float xi = (EBEAM - (EBEAM-particle->E))/EBEAM;
          h_DiffractiveXi->Fill(xi);
          if (xi > 0.02 && xi < 0.12){
              nAFpAccPhotons++;
          }
        }
      } else if (particle->PID > 100){
        h_AlpM->Fill(particle->M);
      }
    }
    h_DiffractiveXiAcc->Fill(nAFpAccPhotons);
    
    // Smear photon ET
    if (doPhotonSmearing){
      for (unsigned int i = 0; i < v_photons.size(); i++){
        float smearing;
        if (fabs(v_photons[i].Eta()) < 0.75){
          smearing = rand->Gaus(1, 0.025);
        } else if (fabs(v_photons[i].Eta()) < 1.25){
          smearing = rand->Gaus(1, 0.035);
        } else if (fabs(v_photons[i].Eta()) < 1.7) {
          smearing = rand->Gaus(1, 0.045);
        } else {
          smearing = rand->Gaus(1, 0.035);
        }
        v_photons[i].SetPtEtaPhiM(v_photons[i].Pt()*smearing, v_photons[i].Eta(), v_photons[i].Phi(), 0);
      }
    }
    
    
    // Analyse Event
    h_NPhotons->Fill(v_photons.size());
    if (v_photons.size() == 2){
      h_AveragePhotonPT->Fill((v_photons[0].Pt()+v_photons[1].Pt())/2);
      
    }
    if (v_photons.size() >= 2){
      h_DiPhoton_M->Fill((v_photons[0]+v_photons[1]).M());
      float dPhi = fabs(v_photons[0].DeltaPhi(v_photons[1]));
      float dEta = fabs(v_photons[0].Eta() - v_photons[1].Eta());
      float aco = 1-(dPhi/TMath::Pi());
      float dR = sqrt((dPhi*dPhi) + (dEta*dEta));
      h_DiPhoton_pT->Fill((v_photons[0]+v_photons[1]).Pt());
      h_DiPhoton_Y->Fill((v_photons[0]+v_photons[1]).Rapidity());
      h_DiPhoton_pz->Fill((v_photons[0]+v_photons[1]).Pz());
      h_DiPhoton_dPhi->Fill(dPhi);
      h_DiPhoton_dEta->Fill(dEta);
      h_DiPhoton_Aco->Fill(aco);
      h_DiPhoton_dR->Fill(dR);
      h_DiPhoton_M_Y->Fill(fabs((v_photons[0]+v_photons[1]).M()), (v_photons[0]+v_photons[1]).Rapidity());
      h_DiPhoton_M_Pz->Fill((v_photons[0]+v_photons[1]).M(), (v_photons[0]+v_photons[1]).Pz());
      h_DiPhoton_M_Pt->Fill((v_photons[0]+v_photons[1]).M(), (v_photons[0]+v_photons[1]).Pt());
      h_DiPhoton_M_Aco->Fill((v_photons[0]+v_photons[1]).M(), aco);
 
/*     
      if ( v_photons[0].E() > 5 && v_photons[1].E() > 5 && fabs(v_photons[0].Eta()) < 4.7 && fabs(v_photons[1].Eta()) < 4.7) {
        h_DiPhoton_M_P5_Eta47->Fill((v_photons[0]+v_photons[1]).M());
      }
      if (v_photons[0].E() > 5 && v_photons[1].E() > 5 && fabs(v_photons[0].Eta()) < 2.5 && fabs(v_photons[1].Eta()) < 2.5) {
        h_DiPhoton_M_P5_Eta25->Fill((v_photons[0]+v_photons[1]).M());
      }
      if ( v_photons[0].E() > 4 && v_photons[1].E() > 4 && fabs(v_photons[0].Eta()) < 4.7 && fabs(v_photons[1].Eta()) < 4.7) {
        h_DiPhoton_M_P4_Eta47->Fill((v_photons[0]+v_photons[1]).M());
      }
      if (v_photons[0].E() > 4 && v_photons[1].E() > 4 && fabs(v_photons[0].Eta()) < 2.5 && fabs(v_photons[1].Eta()) < 2.5) {
        h_DiPhoton_M_P4_Eta25->Fill((v_photons[0]+v_photons[1]).M());
      }
      if ( v_photons[0].E() > 1 && v_photons[1].E() > 1 && fabs(v_photons[0].Eta()) < 4.7 && fabs(v_photons[1].Eta()) < 4.7) {
        h_DiPhoton_M_P1_Eta47->Fill((v_photons[0]+v_photons[1]).M());
      }
      if (v_photons[0].E() > 1 && v_photons[1].E() > 1 && fabs(v_photons[0].Eta()) < 2.5 && fabs(v_photons[1].Eta()) < 2.5) {
        h_DiPhoton_M_P1_Eta25->Fill((v_photons[0]+v_photons[1]).M());
      }
      if (v_photons[0].E() > 2 && v_photons[1].E() > 2 && fabs(v_photons[0].Eta()) < 2.5 && fabs(v_photons[1].Eta()) < 2.5) {
        h_DiPhoton_M_P2_Eta25->Fill((v_photons[0]+v_photons[1]).M());
      }
      if (v_photons[0].E() > 3 && v_photons[1].E() > 3 && fabs(v_photons[0].Eta()) < 2.5 && fabs(v_photons[1].Eta()) < 2.5) {
        h_DiPhoton_M_P3_Eta25->Fill((v_photons[0]+v_photons[1]).M());
      }
*/
    }
    m_outTree->Fill();
  }
}

void LHETruthAnalysis::finalizeHistograms()
{
  // norm eff histos
  h_DiffractiveXiAcc->Scale(1.0/treeReader->GetEntries());
  
  TFile *outFile = new TFile(m_outFile.c_str(), "RECREATE");
  if (!outFile->IsOpen()){
    std::cout << "ERROR: could not open file for writing "<<m_outFile<<std::endl;
    return;
  }
/*
  // Calculate Acceptances
  h_DiPhoton_M_P5_Eta25->Sumw2();
  //h_DiPhoton_M_P5_Eta25->Divide(h_DiPhoton_M);
  h_DiPhoton_M_P5_Eta47->Sumw2();
  //h_DiPhoton_M_P5_Eta47->Divide(h_DiPhoton_M);
  h_DiPhoton_M_P4_Eta25->Sumw2();
  //h_DiPhoton_M_P4_Eta25->Divide(h_DiPhoton_M);
  h_DiPhoton_M_P4_Eta47->Sumw2();
  //h_DiPhoton_M_P4_Eta47->Divide(h_DiPhoton_M);
  h_DiPhoton_M_P1_Eta25->Sumw2();
  h_DiPhoton_M_P1_Eta47->Sumw2();
*/
  for (auto hist : m_histos){
    hist->SetDirectory(outFile);
  }
  for (auto hist : m_histos2D){
    hist->SetDirectory(outFile);
  }
  outFile->Write();
  outFile->Close();
  m_outFileNTUP->Write();
  m_outFileNTUP->Close();
}

void LHETruthAnalysis::run(std::vector<std::string> inFiles, std::string outFile) {
  if (outFile != ""){
    m_outFile = outFile;
  }
  connectBranches(inFiles);
  createHistograms();
  eventLoop();
  finalizeHistograms();
}


/** main() 
 * handle command line parameters and run analysis
 */
int main(int argc, char **argv)
{
  // process command line arguemnts
  int PDFStartMem = -1;
  int PDFEndMem = -1;
  int dumpNEvents = 0;
  std::string outFile("Output.root");
  std::string outFilePostfix("");
  std::vector<std::string> inFiles;
  for ( int arg = 1; arg < argc; arg++){
    if (strcmp(argv[arg], "-pdfStartMem") == 0) {
      arg++;
      PDFStartMem = atof(argv[arg]);
    } else if (strcmp(argv[arg], "-pdfEndMem") == 0) {
      arg++;
      PDFEndMem = atof(argv[arg]);
    } else if (strcmp(argv[arg], "-tag") == 0) {
      arg++;
      outFilePostfix = argv[arg];
    } else if (strcmp(argv[arg], "-f") == 0) {
      arg++;
      inFiles.push_back(argv[arg]);
    } else if (strcmp(argv[arg], "-outFile") == 0) {
      arg++;
      outFile = argv[arg];
    } else if (strcmp(argv[arg], "-PrintNEvents") == 0) {
      arg++;
      dumpNEvents = atoi(argv[arg]);
    } else {
      cout << "Unknown command line arg: "<<argv[arg]<<endl;
      return 1;
    }
  }
  
  LHETruthAnalysis *ana = new LHETruthAnalysis();
  ana->SetDumpNEvents(dumpNEvents);
  ana->run(inFiles, outFile);
}
    
